module archiveBalancer

go 1.16

require (
	code.cloudfoundry.org/bytefmt v0.0.0-20210608160410-67692ebc98de
	github.com/cheggaaa/pb/v3 v3.0.8
	github.com/ricochet2200/go-disk-usage/du v0.0.0-20210707232629-ac9918953285
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
