package main

import (
"bytes"
"crypto/sha256"
"fmt"
"hash"
"io"
"io/ioutil"
"log"
"os"
"path/filepath"
"time"

"code.cloudfoundry.org/bytefmt"
"github.com/cheggaaa/pb/v3"
"github.com/ricochet2200/go-disk-usage/du"
"gopkg.in/yaml.v3"
)

func main() {
	// Load configuration
	config, err := loadConfig()
	if err != nil {
		log.Fatalf("Could not load configuration: %v", err)
	}

	for {
		if err = app(config); err != nil {
			log.Printf("Failed to process files! %v", err)
		}
	}
}

type config struct {
	Source string   `yaml:"source_directory"`
	Pattern string	`yaml:"match_pattern"`
	Disks  []string `yaml:"destination_directories"`
	Delay  int      `yaml:"delay_minutes"`
	ProgressBar  bool	`yaml:"progress"`
}

func (c *config) getConf(path string) *config {
	yamlFile, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatalf("Could not read configuration file: %v", err)
	}

	err = yaml.Unmarshal(yamlFile, &c)
	if err != nil {
		log.Fatalf("Could not unmarshal configuration file: %v", err)
	}

	return c
}

func loadConfig() (*config, error) {
	homeDir, err := os.UserHomeDir()
	if err != nil {
		return nil, fmt.Errorf("could not populate user's home directory: %w", err)
	}
	confDir := filepath.Join(homeDir, ".config","archivebalancer")

	err = os.MkdirAll(confDir, 0770)
	if err != nil {
		return nil, fmt.Errorf("could not create configuration directory: %w", err)
	}

	confFile := filepath.Join(confDir, "conf.yaml")
	log.Printf("Loading configuration from [%s]\n", confFile)
	if _, err := os.Stat(confFile); os.IsNotExist(err) {
		// path/to/whatever does not exist
		log.Printf("Configuration should be located in [%s]\n", confFile)
		d, err := yaml.Marshal(config{
			Source: "/mnt/archivetemp",
			Pattern: "*.*",
			Disks:  []string{"/mnt/archive/drive1","/mnt/archive/drive2","/mnt/archive/drive3"},
			Delay:  10,
		})
		if err != nil {
			log.Fatalf("could not marshal example config: %v", err)
		}
		log.Fatalf("Sample Config:\n%s\n\n", string(d))
	} else if err != nil {
		return nil, fmt.Errorf("could not determine config file exists: %w", err)
	}

	conf := new(config)
	return conf.getConf(confFile), nil
}

func app(config *config) error {
	log.Println("Checking for new files...")
	// Scan for new plots
	tempFiles, err := checkNewFiles(config)
	if err != nil {
		return fmt.Errorf("could not check for new files: %w", err)
	}

	log.Printf("Found [%d] new files\n", len(tempFiles))
	if len(tempFiles) == 0 {
		time.Sleep(time.Duration(config.Delay) * time.Minute)
		return nil
	}

	for _, tempFile := range tempFiles {
		log.Printf("Processing file [%s]", tempFile.Name())
		if err = moveFile(config, tempFile); err != nil {
			return fmt.Errorf("could not move file: %w", err)
		}
	}

	log.Println("Finished processing new files.")

	return nil
}

func checkNewFiles(config *config) ([]os.FileInfo, error) {
	return WalkMatch(config.Source, config.Pattern)
}

func WalkMatch(root, pattern string) ([]os.FileInfo, error) {
	log.Printf("Scanning [%s]...", root)
	var matches []os.FileInfo
	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return fmt.Errorf("could not scan source directory: %w", err)
		}
		if info.IsDir() {
			return nil
		}
		if !info.Mode().IsRegular() {
			return nil
		}
		if matched, err := filepath.Match(pattern, filepath.Base(path)); err != nil {
			return fmt.Errorf("could not filter path: %w", err)
		} else if matched {
			matches = append(matches, info)
		}
		return nil
	})
	if err != nil {
		return nil, fmt.Errorf("failed to scan source directory: %w", err)
	}
	return matches, nil
}

func moveFile(config *config, file os.FileInfo) error {
	log.Println("Checking that file does not already exist")
	for _, drive := range config.Disks {
		driveFile := filepath.Join(drive, file.Name())
		if _, err := os.Stat(driveFile); !os.IsNotExist(err) {
			log.Printf("File found on [%s]!\n", drive)

			sourceHash, err := hashFile(filepath.Join(config.Source, file.Name()), config.ProgressBar)
			if err != nil {
				return fmt.Errorf("could not hash temp file: %w", err)
			}

			destinationHash, err := hashFile(driveFile, config.ProgressBar)
			if err != nil {
				return fmt.Errorf("could not hash temp file: %w", err)
			}

			if bytes.Compare(sourceHash.Sum(nil), destinationHash.Sum(nil)) != 0 {
				log.Printf("File hashes do not match, deleting file on [%s]\n", drive)
				if err = os.Remove(driveFile); err != nil {
					return fmt.Errorf("could not delete file [%s]: %w", file.Name(), err)
				}
			}
		}
	}

	log.Println("Checking drive freespace...")
	drive, space, err := scanDrives(config)
	if err != nil {
		return fmt.Errorf("could not scan available drive freespace: %w", err)
	}

	// Check that new file will fit on drive
	if uint64(file.Size()) > space {
		return fmt.Errorf("new file [%s] needs [%s] more available drive space [%s]",
			bytefmt.ByteSize(uint64(file.Size())),
			bytefmt.ByteSize(uint64(file.Size()) - space),
			bytefmt.ByteSize(space))
	}

	log.Printf("Plot can be moved to [%s] with [%s] available\n", drive, bytefmt.ByteSize(space))

	// Copy new file to drive with temp file ext
	sourcePlot := filepath.Join(config.Source, file.Name())
	source, err := os.Open(sourcePlot)
	if err != nil {
		return fmt.Errorf("could not open source file: %w", err)
	}

	tempPlot := filepath.Join(drive, file.Name() + ".tmp")
	destination, err := os.Create(tempPlot)
	if err != nil {
		return fmt.Errorf("could not open temp file: %w", err)
	}

	log.Println("Copying file to temp file...")

	sourceHash := sha256.New()
	data := io.TeeReader(source, sourceHash)
	if err = multiPartCopy(data, destination, file.Size(), config.ProgressBar); err != nil {
		return fmt.Errorf("could not copy file to new drive: %w", err)
	}
	if err = source.Close(); err!= nil {
		return fmt.Errorf("could not close source file! %w", err)
	}
	if err = destination.Close(); err!= nil {
		return fmt.Errorf("could not close temp file! %w", err)
	}

	log.Println("Verifying file hash...")
	// Verify temp file
	destinationHash, err := hashFile(tempPlot, config.ProgressBar)
	if err != nil {
		return fmt.Errorf("could not hash temp file: %w", err)
	}

	if bytes.Compare(sourceHash.Sum(nil), destinationHash.Sum(nil)) != 0 {
		err = os.Remove(tempPlot)
		return fmt.Errorf("source and destination file hash do not match! %w", err)
	}

	// Rename temp file
	if err = os.Rename(tempPlot, filepath.Join(drive, file.Name())); err != nil {
		return fmt.Errorf("could not rename temp file: %w", err)
	}

	// Delete source file
	log.Println("Deleting source file.")
	if err = os.Remove(sourcePlot); err != nil {
		return fmt.Errorf("could not remove source file: %w", err)
	}

	return nil
}

func scanDrives(config *config) (string, uint64, error){
	var plotDrive string
	var freeSpace uint64

	// Scan for available drives
	for _, tempDrive := range config.Disks {
		tempUsage := du.NewDiskUsage(tempDrive)
		if tempUsage.Available() > freeSpace {
			plotDrive = tempDrive
			freeSpace = tempUsage.Available()
		}
	}
	if plotDrive == "" {
		return "", 0, fmt.Errorf("no drives available")
	}
	log.Printf("Found drive [%s] with [%s] freespace.\n", plotDrive, bytefmt.ByteSize(freeSpace))

	return plotDrive, freeSpace, nil
}

func multiPartCopy(source io.Reader, destination io.Writer, size int64, enablePB bool) error {
	// buf := make([]byte, 1000000)
	// start new progress bar
	if enablePB {
		bar := pb.Full.Start64(size)
		defer bar.Finish()
		barReader := bar.NewProxyReader(source)
		_, err := io.Copy(destination, barReader)
		return err
	}
	// for {
	// 	n, err := barReader.Read(buf)
	// 	if err != nil && err != io.EOF {
	// 		return err
	// 	}
	// 	if n == 0 {
	// 		break
	// 	}
	//
	// 	if _, err := destination.Write(buf[:n]); err != nil {
	// 		return err
	// 	}
	// }

	_, err := io.Copy(destination, source)
	return err
}

func hashFile(path string, enablePB bool) (hash.Hash, error) {
	fileHash := sha256.New()
	file, err := os.Open(path)
	if err != nil {
		return nil, fmt.Errorf("could not open temp file: %w", err)
	}

	info, err := file.Stat()
	if err != nil {
		return nil, fmt.Errorf("could not get file size: %w", err)
	}

	if err = multiPartCopy(file, fileHash, info.Size(), enablePB); err != nil {
		return nil, fmt.Errorf("could not hash temp file: %w", err)
	}

	return fileHash, nil
}
